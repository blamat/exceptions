﻿namespace Exceptions
{
    using System;

    internal sealed class Logic
    {
        public int DoTheMagic(int a, int b)
        {

            try
            {
                if (a > b)
                {
                    return this.MethodA(a, b);
                }
                return -this.MethodA(a, b);
            }catch(Exception ex)
            {
                throw new ModuleException(ex);
            }
        }



        private int MethodA(int a, int b)
        {
            return 5 * this.MethodB(a, b);
        }

        private int MethodB(int a, int b)
        {
            return 10 * this.MethodC(a, b) + 511;
        }

        private int MethodC(int a, int b)
        {
            return a / b; // Possible DivideByZeroException
        }
    }
}
