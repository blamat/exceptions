﻿using System;

namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                OptionA_DontCatch();
                //OptionB_ReturnDefaultValueWhenException();
                //OptionC_LogAndRethrow();
                //OptionD_LogAndThrowNew();
                //OptionE_LogAndThrowNative();
                //OptionF_LogCatchOnlyNatives();
            }
            catch (Exception ex)
            {
                string[] lines = ex.StackTrace.Split('\n');
                int i = 1;
                Console.WriteLine();
                foreach (var s in lines)
                {
                    Console.WriteLine("*** [" + i + "]" );
                    Console.WriteLine(s);
                    Console.WriteLine();
                    i++;
                }

                Console.WriteLine(ex.InnerException == null ? "*** [NO INNER EXCEPTION]" : "*** [INNER]: "+ex.InnerException.GetType().Name);
            }

            Console.ReadLine();
        }

        // ---------------------------------------------------------------------------------------

        private static int OptionA_DontCatch()
        {
            var logic = new Logic();
            return logic.DoTheMagic(1, 0);
        }

        // ---------------------------------------------------------------------------------------

        private static int OptionB_ReturnDefaultValueWhenException()
        {
            try
            {
                var logic = new Logic();
                return logic.DoTheMagic(1, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Some kind of log]:" + ex.Message);
                return -1; 
            }
        }

        // ---------------------------------------------------------------------------------------

        private static int OptionC_LogAndRethrow()
        {
            try
            {
                var logic = new Logic();
                return logic.DoTheMagic(1, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Some kind of log]:"+ex.Message);
                throw;
            }
        }

        // ---------------------------------------------------------------------------------------

        private static int OptionD_LogAndThrowNew()
        {
            try
            {
                var logic = new Logic();
                return logic.DoTheMagic(1, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Some kind of log]:" + ex.Message);
                throw ex;
            }
        }

        // ---------------------------------------------------------------------------------------

        private static int OptionE_LogAndThrowNative()
        {
            try
            {
                var logic = new Logic();
                return logic.DoTheMagic(1, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Some kind of log]:" + ex.Message);
                throw new ModuleException(ex);
            }
        }
    
        // ---------------------------------------------------------------------------------------

        private static int OptionF_LogCatchOnlyNatives()
        {
            try
            {
                var logic = new Logic();
                return logic.DoTheMagic(1, 0);
            }
            catch (ModuleException ex)
            {
                Console.WriteLine("[Some kind of log]:" + ex.Message);
                throw;
            }
        }

        // ---------------------------------------------------------------------------------------
    }
}
