﻿using System;

namespace Exceptions
{
    public class ModuleException : Exception
    {
        public ModuleException(Exception ex)
            : base("ModuleException occured.", ex)
        {
        }
    }
}
